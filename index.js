// Setup the dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")

// Allows all the user routes created in "userRoutes.js"
const userRoute = require("./routes/userRoute")
// Server setup
const app = express()

// Middlewares
app.use(cors())
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use("/users", userRoute)

// Database connection
mongoose.connect("mongodb+srv://psanhi24:admin123@zuitt-bootcamp.asvy4zs.mongodb.net/courseBookingAPI?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	})
mongoose.connection.once("open",() => console.log(`Now connected to the cloud database!`))

// Server listening
// Will used the define port number for the application whenever environment variable is available or used port 400 if none is defined
// This syntax will allow frlexibility when using the application locally or as a hosted application
app.listen(process.env.PORT || 4000, () => console.log(`Now connected to port ${process.env.PORT || 4000}`))