const User = require("../models/User");

// Check if email already exists
/*
Business Logic:
1. Use mongoose "find" method to find duplicate emails
2 .Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result =>{
		if(result.length > 0){
			return true
		}else{
			return false
		}
	})
}